package com.example.management.api.transfare;

import com.example.management.api.transfare.aop.CustomerNotFoundException;
import com.example.management.api.transfare.transaction.Invoice;
import com.example.management.api.transfare.transaction.InvoiceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * The type Customer service.
 */
@Service
@Transactional
@AllArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final InvoiceRepository invoiceRepository;

    /**
     * Add.
     *
     * @param dto the dto
     */
    public void add(AddCustomerDto dto) {
        Date currentDate = new Date();
        Customer customer = new Customer().setFirstName(dto.getFirstName())
                                          .setLastName(dto.getLastName())
                                          .setCreatedDate(currentDate)
                                          .setUpdatedDate(currentDate);
        customerRepository.save(customer);

        for (Double price : dto.getItemsPrice())
            invoiceRepository.save(new Invoice().setAmount(price)
                                                .setCustomer(customer)
                                                .setCreatedDate(currentDate)
                                                .setUpdatedDate(currentDate));
    }

    /**
     * View customer dto . view.
     *
     * @param customerId the customer id
     * @return the customer dto . view
     */
    public ViewCustomerDto view(long customerId) {
        Customer customer = customerRepository.findById(customerId)
                                              .orElseThrow(() -> new CustomerNotFoundException(customerId));

        List<Invoice> invoices = invoiceRepository.findByCustomer_Id(customerId);

        ViewCustomerDto customerDto = new ViewCustomerDto().setFirstName(customer.getFirstName())
                                                           .setLastName(customer.getLastName())
                                                           .setCreatedDate(new SimpleDateFormat("dd/MM/yyyy").format(customer.getCreatedDate()))
                                                           .setUpdateDateDate(new SimpleDateFormat("dd/MM/yyyy").format(customer.getUpdatedDate()));

        for (Invoice invoice : invoices)
            customerDto.getInvoices()
                       .add(new ViewInvoiceDto().setPrice(invoice.getAmount())
                                                .setCreatedDate(new SimpleDateFormat("dd/MM/yyyy").format(invoice.getCreatedDate()))
                                                .setUpdateDateDate(new SimpleDateFormat("dd/MM/yyyy").format(invoice.getUpdatedDate())));
        return customerDto;
    }

    /**
     * Delete.
     *
     * @param customerId the customer id
     */
    public void delete(long customerId) {
        Customer customer = customerRepository.findById(customerId)
                                              .orElseThrow(() -> new CustomerNotFoundException(customerId));
        customerRepository.delete(customer);
    }
}