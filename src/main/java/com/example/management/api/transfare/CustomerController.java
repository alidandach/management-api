package com.example.management.api.transfare;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * The type Customer controller.
 */
@RestController
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    /**
     * Add.
     *
     * @param dto the dto
     */
    @PostMapping
    public void add(@RequestBody AddCustomerDto dto) {
        customerService.add(dto);
    }

    /**
     * View customer dto . view.
     *
     * @param customerId the customer id
     * @return the customer dto . view
     */
    @GetMapping("/{customerId}")
    public ViewCustomerDto view(@PathVariable("customerId") long customerId) {
        return customerService.view(customerId);
    }

    /**
     * Delete.
     *
     * @param customerId the customer id
     */
    @DeleteMapping("/{customerId}")
    public void delete(@PathVariable("customerId") long customerId) {
        customerService.delete(customerId);
    }
}