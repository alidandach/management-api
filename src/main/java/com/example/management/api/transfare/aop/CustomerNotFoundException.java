package com.example.management.api.transfare.aop;

/**
 * The type Customer not found exception.
 */
public class CustomerNotFoundException extends RuntimeException {
    /**
     * Instantiates a new Customer not found exception.
     *
     * @param customerId the customer id
     */
    public CustomerNotFoundException(long customerId) {
        super("Customer " + customerId + " Not Found");
    }
}
