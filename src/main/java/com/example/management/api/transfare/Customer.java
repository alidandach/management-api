package com.example.management.api.transfare;

import com.example.management.api.transfare.transaction.Invoice;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * The type Customer.
 */
@Getter
@Setter
@Entity
@Table(name = "customer")
@Accessors(chain = true)
public class Customer {
    /*
     * Customer ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /*
     * First Name
     */
    @Column(name = "first_name", nullable = false)
    private String firstName;

    /*
     *Last Name
     */
    @Column(name = "last_name", nullable = false)
    private String lastName;


    /**
     * Customer Created Date
     */
    @Column(name = "created_date", nullable = false)
    private Date createdDate;

    /**
     * Customer Updated Date
     */
    @Column(name = "updated_date", nullable = false)
    private Date updatedDate;

    /**
     * Relation With Invoice
     */
    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Invoice> invoices;
}
