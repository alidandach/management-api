package com.example.management.api.transfare.aop;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class GlobalExceptionController {
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {HttpMessageConversionException.class})
    public String badHttpParsing(HttpMessageConversionException exception) {
        log.warn(exception.getMessage());
        return exception.getMessage();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public String httpMethodNotSupported(HttpRequestMethodNotSupportedException exception) {
        log.warn(exception.getMessage());
        return exception.getMessage();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = CustomerNotFoundException.class)
    public String exception(CustomerNotFoundException exception) {
        log.warn(exception.getMessage());
        return exception.getMessage();
    }
}